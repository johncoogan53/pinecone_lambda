use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use aws_config::{self, BehaviorVersion, Region};
use aws_sdk_secretsmanager;
use log::{error, LevelFilter};
use reqwest::Client;
use serde_json::Value;
//use std::env;


const INDEX_HOST: &str = "cocktails-u0h7tr0.svc.aped-4627-b74a.pinecone.io";


async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    //secret extraction for pinecone
    let secret_name = "PINECONE_API_KEY";
    let region = Region::new("us-east-1");
     
    let config = aws_config::defaults(BehaviorVersion::v2023_11_09())
        .region(region)
        .load()
        .await;
    
    let asm = aws_sdk_secretsmanager::Client::new(&config);
    
    let response = asm
        .get_secret_value()
        .secret_id(secret_name)
        .send()
        .await?;
    
    
    let secret_string = response.secret_string();
    let json_data: Value = match secret_string {
        Some(json_str) => serde_json::from_str(json_str)?,
        None => serde_json::Value::Null, // or any other default value as needed
    };
    let api_key = json_data["PINECONE_API_KEY"].as_str().unwrap_or_default();
    error!("response complete: {}",secret_string.unwrap_or_default());
    
    
    
    // Extract Cocktail Name from Request
    let name = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("name"))
        .map_or("NA", |name| name);
    error!("Name Recieved: {}",name);

    let fetch_url = format!("https://{}/vectors/fetch?ids={}", INDEX_HOST, &name);
    let fetch_response = Client::new()
        .get(&fetch_url)
        .header("Api-Key",api_key)
        .send()
        .await;
    error!("Fetch Response: {:?}", fetch_response);
    let matches: String;
    match fetch_response {
        Ok(response) => {
            let body = response.text().await.unwrap();
            error!("Response from Pinecone API: {}", body);

            let json_data: serde_json::Value = serde_json::from_str(&body).unwrap();
            let vectors = extract_values(&json_data, &name).unwrap();
            error!("Vector Values: {:?}", vectors);
            // Perform the query
            let query_url = &format!("https://{}/query", INDEX_HOST);
            let query_data = serde_json::json!({
                "vector": vectors,
                "topK": 3,
                "includeValues": false,
                "includeMetadata": true
            });
            error!("Query Data: {:?}", query_data);
            let query_response = Client::new()
                .post(query_url)
                .header("Api-Key", api_key)
                .header("Content-Type", "application/json")
                .body(query_data.to_string())
                .send()
                .await;
            error!("Query Response: {:?}", query_response);
            matches = match query_response {
                Ok(response) => {
                    let body = response.text().await.unwrap();
                    error!("Response from Pinecone API: {}", body);
                    body
                }
                Err(_) => todo!(),
            };


        }
        Err(_) => todo!()
    }

    
    

    
    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::Text(matches))
        .map_err(Box::new)?;
    Ok(resp)

}

fn extract_values(json_data: &Value, drink_name: &str) -> Option<Vec<f64>> {
    if let Some(vectors) = json_data["vectors"].as_object() {
        if let Some(drink) = vectors.get(drink_name) {
            if let Some(values) = drink["values"].as_array() {
                let values_vec: Vec<f64> = values
                    .iter()
                    .filter_map(|v| v.as_f64())
                    .collect();
                return Some(values_vec);
            }
        }
    }
    None
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    env_logger::builder()
        .filter_level(LevelFilter::Error) // Set log level filter to include info logs
        .init(); 
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
